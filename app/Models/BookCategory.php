<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookCategory extends Model
{
    protected $table = 'book_category';
    
    protected $fillable = ['name'];

    public function book(){
        return $this->hasMany('App\Models\Book', 'category_id');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'book';

    protected $fillable = [
        'title', 'description', 'category', 'keywords', 'price', 'stock', 'publisher'
    ];

    public function category(){
        return $this->belongsTo('App\Models\BookCategory');
    }
}

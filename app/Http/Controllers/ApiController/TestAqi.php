<?php

namespace App\Http\Controllers\ApiController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class TestAqi extends Controller
{
    public function userData()
    {
        $data = User::all();
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }
}

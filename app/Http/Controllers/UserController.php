<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Hash;


class UserController extends Controller
{
    private $user;
    function __construct(User $user, DataTables $dataTable)
    {   
        $this->user = $user;
        $this->dataTable = $dataTable;
    }

    public function create() {
        $model = new User();
        return view('users.form', compact('model'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:8'
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        return response()->json(User::create($input));
    }

    public function edit($id) {
        $model = User::find($id);
        return view('users.form', compact('model'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
        ]);
        $input = $request->all();
        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = \Arr::except($input,array('password'));    
        }

        $model = User::find($id);
        return response()->json($model->update($input));
    }

    public function destroy($id){
        return response()->json(User::find($id)->delete());
    }


    public function userDataTable()
    {
        return $this->dataTable->of($this->user->all())
            ->addIndexColumn()
            ->toJson();
    }
}

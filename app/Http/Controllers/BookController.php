<?php

namespace App\Http\Controllers;

use App\Models\Book;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;

class BookController extends Controller
{
    private $book;

    function __construct(Book $book, DataTables $dataTable)
    {   
        $this->book = $book;
        $this->dataTable = $dataTable;
    }

    public function index()
    {
        return view('book.index');
    }

    public function create()
    {
        $model = new $this->book;
        $category = DB::table('book_category')->pluck('name', 'name');
        return view('book.form', compact('model', 'category'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
            'keywords' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'publisher' => 'required',
        ]);
        
        $input = $request->all();
        $input['category'] = implode(', ', $request->category);
        $model = $this->book->create($input);
        return response()->json($model);
    }

    public function show($id)
    {
        $model = $this->book->findOrFail($id);
        $category = DB::table('book_category')->pluck('name', 'id');
        return view('book.form', compact('model', 'category'));
    }

    public function edit($id)
    {
        $model = $this->book->findOrFail($id);
        $category = DB::table('book_category')->pluck('name', 'name');
        return view('book.form', compact('model', 'category'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'category' => 'required',
            'keywords' => 'required',
            'price' => 'required',
            'stock' => 'required',
            'publisher' => 'required',
        ]);
        
        $input = $request->all();
        $input['category'] = implode(', ', $request->category);

        $model = $this->book->findOrFail($id);
        
        return response()->json($model->update($input));
    }

    public function destroy($id)
    {
        $model = $this->book->findorFail($id);

        return response()->json($model->delete());
    }

    public function bookDataTable()
    {

        return $this->dataTable->of($this->book->all())
            ->addIndexColumn()
            ->toJson();
    }
}

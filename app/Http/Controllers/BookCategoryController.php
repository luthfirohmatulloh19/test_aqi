<?php

namespace App\Http\Controllers;

use App\Models\BookCategory;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class BookCategoryController extends Controller
{
    private $bookCategory;

    function __construct(BookCategory $bookCategory, DataTables $dataTable)
    {   
        $this->bookCategory = $bookCategory;
        $this->dataTable = $dataTable;
    }

    public function index()
    {
        return view('book_category.index');
    }


    public function create()
    {
        $model = new $this->bookCategory;
        return view('book_category.form', compact('model'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        
        $model = $this->bookCategory->create($request->all());
        return response()->json($model);
    }

    public function show($id)
    {
        $model = $this->bookCategory->findorFail($id);
        return view('book_category.form', compact('model'));  
    }

    public function edit($id)
    {
        $model = $this->bookCategory->findorFail($id);
        return view('book_category.form', compact('model'));   
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required'
        ]);
        
        $model = $this->bookCategory->findorFail($id);

        return response()->json($model->update($request->all()));
    }

    public function destroy($id)
    {
        $model = $this->bookCategory->findorFail($id);

        return response()->json($model->delete());
    }

    public function bookCategoryDataTable()
    {
        return $this->dataTable->of($this->bookCategory->all())
            ->addIndexColumn()
            ->toJson();
    }
}

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Kategori Buku
                    <a class="btn btn-outline-success modal-show float-right" title="Tambah Data" href="{{ route('book_category.create') }}"> Tambah Data</a>
                </div>


                <div class="card-body" id="book-category">
                    <div class="table-responsive py-8">
                        <input type="hidden" id="data-url" value="{{ route('book_category.table') }}"/>
                        <table class="table tablese table-flush" id="book-category-table" style="width:100%;">
                            <thead class="thead-light">
                                <tr>
                                    <th style="width:5%;">No</th>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th style="width:5%;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection




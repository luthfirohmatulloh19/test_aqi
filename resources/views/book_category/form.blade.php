{!! Form::model($model, [
    'route' => $model->exists ? ['book_category.update', $model->id] : 'book_category.store',
    'id' => 'test-aqi-form'  
]) !!}
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Nama *</label>
                {!! Form::text('name',null,['class' => 'form-control', 'id' => 'name']) !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
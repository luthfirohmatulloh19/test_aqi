{!! Form::model($model, [
    'route' => $model->exists ? ['book.update', $model->id] : 'book.store',
    'id' => 'test-aqi-form'  
]) !!}
    <div class="row">
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Judul Buku</label>
                {!! Form::text('title',null,['class' => 'form-control', 'id' => 'title']) !!}
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Deskripsi</label>
                {!! Form::text('description',null,['class' => 'form-control', 'id' => 'description']) !!}
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Kategori</label>
                {!! Form::select('category[]', $category, null,['class' => 'form-control', 'multiple' => 'multiple', 'id' => 'category']) !!}
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Keywords</label>
                {!! Form::text('keywords', null,['class' => 'form-control', 'id' => 'keywords']) !!}
                <small id="keywords" class="form-text text-muted">*) Pisahkan dengan tanda koma (eg. RUP, SCRUM, Agile)</small>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Harga</label>
                {!! Form::number('price',null,['class' => 'form-control', 'id' => 'price', 'min' => '0']) !!}
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Stock</label>
                {!! Form::number('stock',null,['class' => 'form-control', 'id' => 'stock', 'min' => '0']) !!}
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <label class="form-control-label">Penerbit</label>
                {!! Form::text('publisher',null,['class' => 'form-control', 'id' => 'publisher']) !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
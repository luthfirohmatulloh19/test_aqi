$(document).ready(function() {
    $parent = $("#book");
    const manageBook = {
        init: () => {
            manageBook.datatables();
        },
        datatables: () => {
            const url = $('#data-url').val();
            $('.tablese').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'id', name: 'id', searchable: false, visible: false },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description',
                        render: function (data, type, row) {
                        text = '';
                        if(data.length > 100)
                            text = data.slice(0,99) + '......'
                        else
                            text = data
                        return text;
                    }},
                    { data: 'category', name: 'category' },
                    { data: 'keywords', name: 'keywords' },
                    { data: 'price', name: 'price',
                        render: function (data, type, row) {
                        return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(data);
                    }},
                    { data: 'stock', name: 'stock' },
                    { data: 'publisher', name: 'publisher'},
                    {
                        data: null,
                        render: function ( data, type, row ) {
                            // Combine the first and last names into a single table field
                            return '<div class="dropdown"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</a> <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item modal-show btn_view" href="book/'+data.id+'" title="Detail buku">View</a><a class="dropdown-item modal-show btn_edit" title="Edit Data" href="book/'+data.id+'/edit" title="Edit buku">Edit</a><a class="dropdown-item btn_delete" href="book/'+data.id+'">Delete</a></div></div>'
                        }
                    }

                ],
            });
        }
    }

    if ($parent.length) {
        manageBook.init();
    }
})
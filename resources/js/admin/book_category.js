$(document).ready(function() {
    $parent = $("#book-category");
    const manageBookCategory = {
        init: () => {
            manageBookCategory.datatables();
        },
        datatables: () => {
            const url = $('#data-url').val();
            $('.tablese').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'id', name: 'id', searchable: false, visible: false },
                    { data: 'name', name: 'name' },
                    {
                        data: null,
                        render: function ( data, type, row ) {
                            // Combine the first and last names into a single table field
                            return '<div class="dropdown"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</a> <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item modal-show btn_view" href="book_category/'+data.id+'" title="Detail buku">View</a><a class="dropdown-item modal-show btn_edit" title="Edit Data" href="book_category/'+data.id+'/edit" title="Edit kategori">Edit</a><a class="dropdown-item btn_delete" href="book_category/'+data.id+'">Delete</a></div></div>'
                        }
                    }

                ],
            });
        }
    }

    if ($parent.length) {
        manageBookCategory.init();
    }
})
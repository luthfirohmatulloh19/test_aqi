$('body').on('click', '.modal-show', function(event){
    event.preventDefault();

    var me = $(this),
        url = me.attr('href'),
        title = me.attr('title');
        
    $('.modal-title').text(title);
    $('#modal-btn-save').show();
    me.hasClass('btn_view') ? $('#modal-btn-save').hide(): $('#modal-btn-save').show();
    $('#modal-btn-save').text(me.hasClass('btn_edit') ? 'Update' : 'Simpan');

    $.ajax({
        url : url,
        dataType: 'html',
        success: function(response){
            $('.modal-body').html(response);
        }
    });

    $('#modal').modal('show');
});

$('#modal-btn-save').click(function(event){
    event.preventDefault();

    var form = $('#test-aqi-form'),
        url = form.attr('action');
    
    form.find('.help-block').remove();
    form.find('.form-group').removeClass('.text-danger');

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    var formData = new FormData(form[0]);
   
    $.ajax({
        url : url,
        method : 'POST',
        data : formData,
        contentType : false,
        processData : false,
        success : function (response) {
            console.log(response);
            form.trigger('reset'),
            $('#modal').modal('hide'),
            $('.tablese').DataTable().ajax.reload();
            swal.fire(
                'Success!',
                'Data berhasil disimpan.',
                'success'
            )
        },
        error: function(xhr){
            var res = xhr.responseJSON;
            if($.isEmptyObject(res) == false){
                $.each(res.errors, function(key, value){
                    $('#' + key)
                        .closest('.form-group')
                        .addClass('text-danger')
                        .append('<span class="help-block"><strong>' + value + '</strong></span>')
                });
            }
            console.log(res);
        }
    });
});

$('body').on('click', '.btn_delete', function(event){
    event.preventDefault();

    var me = $(this);
        url = me.attr('href'),
        title = me.attr('title'),
        csrf_token = $('meta[name="csrf-token"]').attr('content');
    
    swal.fire({
        title: 'Apakah anda yakin?',
        text: 'Anda tidak akan dapat mengembalikan data ini!!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Tidak'
        }).then((result)=>{
        if(result.value){
            $.ajax({
                url: url,
                type: "POST",
                data: {
                    '_method' : "DELETE",
                    '_token' : csrf_token
                },
                success: function(response){
                    var res = response.responseJSON;
                    $('.tablese').DataTable().ajax.reload();
                    swal.fire(
                        'Deleted!',
                        'Data berhasil dihapus.',
                        'success'
                    )
                },
                error: function(xhr){
                    var res = xhr.responseJSON;
                    swal.fire(
                        'Cancelled',
                        'Data gagal dihapus.',
                        'error'
                    );
                }
            });
        }
    });
});
$(document).ready(function() {
    $parent = $("#book-category");
    const manageBookCategory = {
        init: () => {
            manageBookCategory.datatables();
        },
        datatables: () => {
            const url = $('#data-url').val();
            $('.tablese').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'id', name: 'id', searchable: false, visible: false },
                    { data: 'name', name: 'name' },
                    {
                        data: null,
                        render: function ( data, type, row ) {
                            // Combine the first and last names into a single table field
                            return '<div class="dropdown"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</a> <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item modal-show btn_view" href="book_category/'+data.id+'" title="Detail buku">View</a><a class="dropdown-item modal-show btn_edit" title="Edit Data" href="book_category/'+data.id+'/edit" title="Edit kategori">Edit</a><a class="dropdown-item btn_delete" href="book_category/'+data.id+'">Delete</a></div></div>'
                        }
                    }

                ],
            });
        }
    }

    if ($parent.length) {
        manageBookCategory.init();
    }
})
$(document).ready(function() {
    $parent = $("#book");
    const manageBook = {
        init: () => {
            manageBook.datatables();
        },
        datatables: () => {
            const url = $('#data-url').val();
            $('.tablese').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'id', name: 'id', searchable: false, visible: false },
                    { data: 'title', name: 'title' },
                    { data: 'description', name: 'description',
                        render: function (data, type, row) {
                        text = '';
                        if(data.length > 100)
                            text = data.slice(0,99) + '......'
                        else
                            text = data
                        return text;
                    }},
                    { data: 'category', name: 'category' },
                    { data: 'keywords', name: 'keywords' },
                    { data: 'price', name: 'price',
                        render: function (data, type, row) {
                        return new Intl.NumberFormat('id-ID', { style: 'currency', currency: 'IDR' }).format(data);
                    }},
                    { data: 'stock', name: 'stock' },
                    { data: 'publisher', name: 'publisher'},
                    {
                        data: null,
                        render: function ( data, type, row ) {
                            // Combine the first and last names into a single table field
                            return '<div class="dropdown"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</a> <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item modal-show btn_view" href="book/'+data.id+'" title="Detail buku">View</a><a class="dropdown-item modal-show btn_edit" title="Edit Data" href="book/'+data.id+'/edit" title="Edit buku">Edit</a><a class="dropdown-item btn_delete" href="book/'+data.id+'">Delete</a></div></div>'
                        }
                    }

                ],
            });
        }
    }

    if ($parent.length) {
        manageBook.init();
    }
})
$(document).ready(function() {
    $parent = $("#user");
    const manageUsers = {
        init: () => {
            manageUsers.datatables();
        },
        datatables: () => {
            const url = $('#data-url').val();
            $('.tablese').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url
                },
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false },
                    { data: 'id', name: 'id', searchable: false, visible: false },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    {
                        data: null,
                        render: function ( data, type, row ) {
                            // Combine the first and last names into a single table field
                            return '<div class="dropdown"><a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Aksi</a> <div class="dropdown-menu" aria-labelledby="dropdownMenuLink"><a class="dropdown-item modal-show btn_edit" title="Edit Data" href="user/'+data.id+'/edit" title="Edit data user">Edit</a><a class="dropdown-item btn_delete" href="user/'+data.id+'">Delete</a></div></div>'
                        }
                    }

                ],
            });
        }
    }

    if ($parent.length) {
        manageUsers.init();
    }
})
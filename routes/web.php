<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('user', 'UserController', ['except' => ['update']]);
    Route::post('user/{id}', 'UserController@update')->name('user.update');
    Route::get('user/data/table', 'UserController@userDataTable')->name('user.table');

    Route::resource('book', 'BookController', ['except' => ['update']]);
    Route::post('book/{id}', 'BookController@update')->name('book.update');
    Route::get('book/data/table', 'BookController@bookDataTable')->name('book.table');

    Route::resource('book_category', 'BookCategoryController', ['except' => ['update']]);
    Route::post('book_category/{id}', 'BookCategoryController@update')->name('book_category.update');
    Route::get('book_category/data/table', 'BookCategoryController@bookCategoryDataTable')->name('book_category.table');
});
